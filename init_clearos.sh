#!/bin/sh

#
# Prep the parameters to pass to systemd
#

# Source networking configuration.
. /etc/clearos/network.conf

# Source miniupnpd_clearos.conf configuration.
. /etc/miniupnpd/miniupnpd_clearos.conf

# Determine WAN interface default route
MINIUPNPD_WAN="-i `LC_ALL=C ip -4 route | grep 'default' | sed -e 's/.*dev[[:space:]]*//' -e 's/[[:space:]].*//'`"

# kludge for when WAN goes down to stop miniupnpd failing
if [ "$MINIUPNPD_WAN" = "-i " ]; then
    MINIUPNPD_WAN="-i lo"
fi

# List all the LAN's with the correct config parameter, checking the LANIF's are valid
MINIUPNPD_LANS=""
for LAN in $LANIF; do
    CHECK=$(ip r | grep "$LAN " 2>/dev/null)
    if [ -n "$CHECK" ]; then
        MINIUPNPD_LANS="$MINIUPNPD_LANS-a $LAN "
    fi
done

if [ "$USE_HOTLAN" = "yes" -a ! -z "$HOTIF" ]; then
    CHECK=$(ip r | grep "$HOTIF " 2>/dev/null)
    if [ -n "$CHECK" ]; then
        MINIUPNPD_LANS="$MINIUPNPD_LANS-a $HOTIF "
    fi
fi

# kludge if LAN not yet configured
if [ "$MINIUPNPD_LANS" = "" ]; then
    MINIUPNPD_LANS="-a lo"
fi

systemctl set-environment MINIUPNPD_START_OPTIONS="$MINIUPNPD_WAN $MINIUPNPD_LANS $MINIUPNPD_OPTIONS"

logger -t miniupnp-options "WAN=$MINIUPNPD_WAN LAN=$MINIUPNPD_LANS Options=$MINIUPNPD_OPTIONS"

exit 0



